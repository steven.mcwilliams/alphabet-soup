package com.iamus.alphabet_soup;

/**
 * Encapsulates one linear path within a matrix, as a pair of begin and end locations.  The path can be vertical,
 * horizontal, or diagonal.
 */
public class Path {
    /**
     * Begin location of our path.
     */
    private final Location beginLoc;
    
    /**
     * End location of our path.
     */
    private final Location endLoc;
    
    /**
     * Constructor.
     * 
     * @param beginLoc Sets our beginLoc attribute.
     * @param endLoc Sets our endLoc attribute.
     */
    public Path(Location beginLoc, Location endLoc) {
        this.beginLoc = beginLoc;
        this.endLoc = endLoc;
    }

    /**
     * @return Returns our beginLoc attribute.
     */
    public Location getBeginLoc() {
        return beginLoc;
    }

    /**
     * @return Returns our endLoc attribute.
     */
    public Location getEndLoc() {
        return endLoc;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(beginLoc);
        builder.append(' ');
        builder.append(endLoc);
        return builder.toString();
    }
}
