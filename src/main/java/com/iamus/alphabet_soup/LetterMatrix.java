package com.iamus.alphabet_soup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

/**
 * Encapsulates a matrix of letters that can be searched to find matches for a specified word.  
 */
public class LetterMatrix {
    /**
     * Map of letters pointing to the locations where they appear within the matrix.  This is used to facilitate
     * finding the first letter of a specified word within the matrix, when initiated a search for matches of that
     * word.
     */
    private final Map<Character, List<Location>> letterLocs;
    
    /**
     * Matrix of letters we encapsulate.  The first dimension is the rows, and the second dimension is the columns.
     */
    private final char[][] matrix;
    
    /**
     * Number of columns in our matrix.  This is derived from our matrix attribute, and cached here for convenience.
     */
    private final int numCols;
    
    /**
     * Number of rows in our matrix.  This is derived from our matrix attribute, and cached here for convenience.
     */
    private final int numRows;
    
    /**
     * Constructor.
     * 
     * @param matrix Sets our matrix attribute.  Note that we make a copy in the process, to make it our own.
     */
    public LetterMatrix(char[][] matrix) {
        Objects.requireNonNull(matrix, "matrix");

        // assign a copy of the matrix 
        this.matrix = Arrays.stream(matrix).map(char[]::clone).toArray(char[][]::new);
        
        numRows = matrix.length;
        numCols = (matrix.length == 0) ? 0 : matrix[0].length;

        // build letterLocs map by iterating over our matrix
        letterLocs = new HashMap<>();
        for (int i = 0; i < matrix.length; i++) {
            for (int j = 0; j < matrix[i].length; j++) {
                char letter = matrix[i][j];
                Location loc = new Location(i, j);
                letterLocs.computeIfAbsent(letter, k -> new ArrayList<>()).add(loc);
            }
        }
    }
    
    /**
     * Finds matches for a specified word within our matrix.  A matching sequence of  letters within the matrix can be 
     * vertical (up or down), horizontal (left or right), or diagonal (up or down).  Any embedded whitespace within the 
     * word being searched for is ignored.
     * 
     * @param word Word to search for.  Must not be null.
     * @return List of matching paths found within the matrix.
     */
    public List<Path> findPaths(String word) {      
        Objects.requireNonNull(word, "word");

        // compress out any surrounding/embedded whitespace
        String compressedWord = word.replaceAll("\\s+", "");    

        char firstLetter = compressedWord.charAt(0);

        // get all locs (if any) in matrix where first letter of search word appears
        List<Location> locs = letterLocs.get(firstLetter);      

        // find matching paths starting at each of the locs where first letter of search word appears
        List<Path> allPaths = new ArrayList<>();
        if (locs != null) {
            for (Location loc : locs) {
                List<Path> paths = findPaths(compressedWord, loc);
                allPaths.addAll(paths);
            }
        }
        
        return allPaths;
    }
    
    /**
     * Finds matches for specified word, using the specified location as the starting point.  The word is assumed
     * to have already had whitespace compressed out of it, and the location is assumed to match the first letter of
     * the word in question.
     * 
     * @param compressedWord Word to search for.
     * @param loc Starting point for the search.
     * @return List of matching paths found within the matrix.
     */
    private List<Path> findPaths(String compressedWord, Location loc) {      
        List<Path> paths = new ArrayList<>();
        
        Path path = findPath(compressedWord, new VerticalDownIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        path = findPath(compressedWord, new VerticalUpIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        path = findPath(compressedWord, new HorizontalRightIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        path = findPath(compressedWord, new HorizontalLeftIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        path = findPath(compressedWord, new DiagonalDownIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        path = findPath(compressedWord, new DiagonalUpIterator(loc));
        if (path != null) {
            paths.add(path);
        }
        
        return paths;
    }
    
    /**
     * Finds matches for specified word, using the specified iterator.  The word is assumed to have already had 
     * whitespace compressed out of it, and the iterator is assumed to point to a location that matches the first
     * letter of the word in question.  
     * <p>
     * Note that we use the iterator to traverse in a linear direction (vertically, horizontally, or diagonally
     * depending on the type of iterator used) through the matrix, in an attempt to match all of the letters in the
     * word in question.  
     * <p>
     * If we find a letter that in the sequence that doesn't match, before we have reached the end of the letters in 
     * the word, we return null.  If we traverse beyond the borders of the matrix, before we have reached the end of
     * the letters in the word, we also return null.
     * 
     * @param compressedWord Word to search for.
     * @param iter Iterator to use to search for matching letters in a linear sequence.
     * @return Matching path found, or null if none was found.
     */
    private Path findPath(String compressedWord, Iterator iter) {      
        int wordLen = compressedWord.length();
        Location loc = iter.getLocation();
        
        boolean mismatch = false;           // flags that a mismatched letter has been encountered
        boolean matrixDone = false;         // flags that we have reached the edge of the matrix
        boolean lettersDone = false;        // flags that we have reached the end of our word

        int beginRowIdx = loc.getRowIdx();  // records the row we began iterating from
        int beginColIdx = loc.getColIdx();  // records the column we began iterating from
        int rowIdx = beginRowIdx;           // index of the row we are examining next
        int colIdx = beginColIdx;           // index of the column we are examining next
        int endRowIdx = rowIdx;             // records the last row we examined
        int endColIdx = colIdx;             // records the last column we examined
        int letterIdx = 0;                  // index of the letter in the word we are currently trying to match

        // iterate through matrix trying to match all letters in our word
        while (! mismatch && ! matrixDone && ! lettersDone) {
            mismatch = (compressedWord.charAt(letterIdx) != matrix[rowIdx][colIdx]);
            endRowIdx = loc.getRowIdx();
            endColIdx = loc.getColIdx();
            if (iter.hasNext()) {
                loc = iter.next();
                rowIdx = loc.getRowIdx();
                colIdx = loc.getColIdx();
            } else {
                matrixDone = true;
            }
            letterIdx++;
            if (letterIdx == wordLen) {
                lettersDone = true;
            }
        }
        
        // construct a path if possible based on our search results
        if (! mismatch && lettersDone) {
            Location beginLoc = new Location(beginRowIdx, beginColIdx);
            Location endLoc = new Location(endRowIdx, endColIdx);
            return new Path(beginLoc, endLoc);
        } else {
            return null;
        }
    }
    
    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder("{\n");
        
        builder.append("    matrix={\n");
        for (int i = 0; i < matrix.length; i++) {
            String row = Arrays.toString(matrix[i]);
            builder.append("        ").append("row ").append(i).append(": ").append(row).append('\n');
        }
        builder.append("    }\n");

        builder.append("    letterLocs=").append(letterLocs);
       
        builder.append("\n}");
        
        return builder.toString();
    }
    
    /**
     * Base class that can be used to iterate over a linear sequence of locations within our matrix.  
     * <p>
     * Note that we are not bothering to have this implement java.util.Iterator, since it is only used internally
     * and doesn't need all of the methods in that interface.
     */
    private abstract class Iterator {
        /**
         * Current location we are pointing to.
         */
        Location loc;

        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        Iterator(Location loc) {
            this.loc = loc;
        }

        /**
         * @return Returns a newly constructed location for the next location in our sequence, and updates our
         *     iterator to point to it.
         */
        abstract Location createNext();
        
        /**
         * @return Returns our loc attribute.
         */
        Location getLocation() {
            return loc;
        }

        /**
         * @return Returns true if the next location in our sequence lies within the matrix.
         */
        abstract boolean hasNext();

        /**
         * @return Returns a newly constructed location for the next location in our sequence, and updates our
         *     iterator to point to it.
         */
        Location next() {
            loc = createNext();
            return loc;
        }
    }
    
    /**
     * Extends base iterator to navigate a diagonal-down sequence.
     */
    private class DiagonalDownIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        DiagonalDownIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationDiagonalDown();
        }

        @Override
        boolean hasNext() {
            return ((loc.getRowIdx() + 1) < numRows) && ((loc.getColIdx() + 1) < numCols);
        }
    }
    
    /**
     * Extends base iterator to navigate a diagonal-up sequence.
     */
    private class DiagonalUpIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        DiagonalUpIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationDiagonalUp();
        }

        @Override
        boolean hasNext() {
            return ((loc.getRowIdx() - 1) >= 0) && ((loc.getColIdx() - 1) >= 0);
        }
    }
    
    /**
     * Extends base iterator to navigate a horizontal-right sequence.
     */
    private class HorizontalRightIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        HorizontalRightIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationHorizontalRight();
        }

        @Override
        boolean hasNext() {
            return (loc.getColIdx() + 1) < numCols;
        }
    }
    
    /**
     * Extends base iterator to navigate a horizontal-left sequence.
     */
    private class HorizontalLeftIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        HorizontalLeftIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationHorizontalLeft();
        }

        @Override
        boolean hasNext() {
            return (loc.getColIdx() - 1) >= 0;
        }
    }
    
    /**
     * Extends base iterator to navigate a vertical-down sequence.
     */
    private class VerticalDownIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        VerticalDownIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationVerticalDown();
        }

        @Override
        boolean hasNext() {
            return (loc.getRowIdx() + 1) < numRows;
        }
    }
    
    /**
     * Extends base iterator to navigate a vertical-up sequence.
     */
    private class VerticalUpIterator extends Iterator {
        /**
         * Constructor.
         * 
         * @param loc Sets our loc attribute.
         */
        VerticalUpIterator(Location loc) {
            super(loc);
        }

        @Override
        Location createNext() {
            return loc.createLocationVerticalUp();
        }

        @Override
        boolean hasNext() {
            return (loc.getRowIdx() - 1) >= 0;
        }
    }
}
