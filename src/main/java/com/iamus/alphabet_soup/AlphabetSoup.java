package com.iamus.alphabet_soup;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

/**
 * Entry point for an application that takes an input file defining a matrix of letters, along with a list of words
 * for us to search for within that matrix.  A matching sequence of letters within the matrix can be vertical (up or
 * down), horizontal (left or right), or diagonal (up or down).  The results of the search are printed to System.out.
 * <p>
 * The input file is expected to contain three sections.  The first section is a single line that defines the size of
 * the matrix that is coming, and is in the form "{num-rows}x{num-cols}" (e.g. "3x3").  The section section is multiple 
 * lines, one for each row in the matrix, where each line is a list of white space delimited letters making up the
 * columns of the row in question (e.g. "A B C\nD E F\nG H I" for a 3x3 matrix).  The third section is multiple lines 
 * again, one for each word we are being asked to search for in the matrix (e.g. "ABC\nAIE" to search for the words
 * "ABC" and "AIE").  
 * <p>
 * Note that any embedded whitespace within a word being searched for is ignored.
 * <p>
 * Also note that the verbiage used to print the result her is currently hard coded in English.  I have not bothered
 * to externalize it in a resource bundle for potential internationalization.
 * <p>
 * Refer to the README for more details about this application.
 */
public class AlphabetSoup {
    /**
     * Logger, named after this class.
     */
    private static final Logger LOGGER = Logger.getLogger(AlphabetSoup.class.getName());
    
    /**
     * Entry point for CLI execution of this application.  Prints out the results of searching for a list of words
     * within a specified matrix, as described in the class header documentation.
     * <p>
     * Requires an input filename argument to be specified.  If the wrong number of arguments are specified, we exit 
     * with a value of 2.  If the input file specified cannot be found, or is not a file, we exit with a value of 1.
     * <p>
     * Note that we do not do any validation of the contents of the input file here, and instead assume that it will
     * be in the proper format.
     *
     * @param args Argument array that should consist of a single entry for the name of the input file to use.
     * @throws IOException Thrown if there are any errors reading the input file.
     */
    public static void main(String[] args) throws IOException {
        File file = parseArgs(args);
        
        // load letter matrix and search words list from input file
        LetterMatrix matrix = null;
        List<String> words = null;
        try (Scanner scanner = new Scanner(file)) {
            matrix = loadMatrix(scanner);
            words = loadWords(scanner);
            if (LOGGER.isLoggable(Level.FINE)) {
                LOGGER.fine("matrix=" + matrix);
                LOGGER.fine("words=" + words);
            }
        }       
        
        // search for words in the letter matrix and print the results
        for (String word : words) {
            List<Path> paths = matrix.findPaths(word);

            String pathsCommaSeparated = paths.stream().map(Object::toString).collect(Collectors.joining(","));
            System.out.printf("%s %s\n", word, pathsCommaSeparated); 
        }
    }

    /**
     * Creates and populates a LetterMatrix instance using the specified input file scanner.  The scanner is assumed
     * to be pointing to the start of the section within the file where the number of rows and columns are specified.
     * After this method completes, the scanner will be pointing beyond the section containing the matrix of letters.
     * 
     * @param scanner File input scanner to use.
     * @return A new LetterMatrix instance, populated with the matrix contents from the file.
     */
    private static LetterMatrix loadMatrix(Scanner scanner) {
        // read in first line containing numRows and numCols
        scanner.useDelimiter("[x\n]");
        int numRows = scanner.nextInt();
        int numCols = scanner.nextInt();
    
        // read in subsequent lines containing matrix of letters
        char[][] letters = new char[numRows][numCols];
        scanner.reset();
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                letters[i][j] = scanner.next().charAt(0); 
            }
        }
        scanner.nextLine();
        
        return new LetterMatrix(letters);
    }
        
    /**
     * Creates and populates a list of words to search for, using the specified input file scanner.  The scanner
     * is assumed to be pointing to the start of the section within the file where these words are specified.  After
     * this method completes, the scanner will be pointing beyond the search words section of the file.
     * 
     * @param scanner File input scanner to use.
     * @return A new list of search words, populated using the search word contents from the file.
     */
    private static List<String> loadWords(Scanner scanner) {
        List<String> words = new ArrayList<>();

        while (scanner.hasNextLine()) {
            String line = scanner.nextLine();
            words.add(line);
        }
        
        return words;
    }
        
    /**
     * Parses the input arguments that were passed into the application from the CLI.  
     * <p>
     * Requires an input filename argument to be specified.  If the wrong number of arguments are specified, we exit 
     * with a value of 2.  If the input file specified cannot be found, or is not a file, we exit with a value of 1.
     * 
     * @param args Argument array that should consist of a single entry for the name of the input file to use.
     * @return A new File instance created for the specified input filename.
     */
    private static File parseArgs(String[] args) {
        if (args.length != 1) {
            System.err.printf("usage: java %s [input_file]\n", AlphabetSoup.class.getName()); 
            System.exit(2);
        }
        String fileName = args[0];

        File file = new File(fileName);
        if (! file.exists()) {
            System.err.printf("'%s' not found\n", fileName);
            System.exit(1);
        } else if (! file.isFile()) {
            System.err.printf("'%s' is not a file\n", fileName);
            System.exit(1);
        }        
        
        return file;
    }
}
