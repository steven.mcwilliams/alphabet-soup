package com.iamus.alphabet_soup;

/**
 * Encapsulates one coordinate within our matrix, as a pair of row and column index values.
 */
public class Location {
    /**
     * Index of the column our location refers to within the matrix.
     */
    private final int colIdx;
    
    /**
     * Index of the row our location refers to within the matrix.
     */
    private final int rowIdx;
    
    /**
     * Constructor.
     * 
     * @param rowIdx Sets our rowIdx attribute.
     * @param colIdx Sets our colIdx attribute.
     */
    public Location(int rowIdx, int colIdx) {
        this.rowIdx = rowIdx;
        this.colIdx = colIdx;
    }

    /**
     * @return A new Location instance that is diagonally just below ours.
     */
    public Location createLocationDiagonalDown() {
        return new Location(rowIdx + 1, colIdx + 1);
    }
    
    /**
     * @return A new Location instance that is diagonally just above ours.
     */
    public Location createLocationDiagonalUp() {
        return new Location(rowIdx - 1, colIdx - 1);
    }
    
    /**
     * @return A new Location instance that is horizontally just to the left of ours.
     */
    public Location createLocationHorizontalLeft() {
        return new Location(rowIdx, colIdx - 1);
    }
    
    /**
     * @return A new Location instance that is horizontally just to the rights of ours.
     */
    public Location createLocationHorizontalRight() {
        return new Location(rowIdx, colIdx + 1);
    }
    
    /**
     * @return A new Location instance that is vertically just below ours.
     */
    public Location createLocationVerticalDown() {
        return new Location(rowIdx + 1, colIdx);
    }
    
    /**
     * @return A new Location instance that is vertically just above ours.
     */
    public Location createLocationVerticalUp() {
        return new Location(rowIdx - 1, colIdx);
    }
    
    /**
     * @return Returns our colIdx attribute.
     */
    public int getColIdx() {
        return colIdx;
    }

    /**
     * @return Returns our rowIdx attribute.
     */
    public int getRowIdx() {
        return rowIdx;
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        builder.append(rowIdx);
        builder.append(':');
        builder.append(colIdx);
        return builder.toString();
    }
}