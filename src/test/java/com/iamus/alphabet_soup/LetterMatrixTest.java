package com.iamus.alphabet_soup;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

/**
 * Unit tests for LetterMatrix.
 */
public class LetterMatrixTest {
    /**
     * Reusable empty array of letters that can be used to populate test matrices.
     */
    private static final char[][] EMPTY_LETTERS = {};

    /**
     * Reusable 2x2 array of letters that can be used to populate test matrices.
     */
    private static final char[][] TWO_BY_TWO_LETTERS = {
        {'A', 'B'},
        {'C', 'D'}
    };

    /**
     * Tests findPaths when a null word is passed in.  NPE is expected.
     */
    @Test
    public void findPaths_nullWord_throwsNPE() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(EMPTY_LETTERS);

        // execute test method
        try {
            matrix.findPaths(null);
            Assertions.fail("NPE expected");
        } catch (NullPointerException ex) {
            // expected
        }
    }

    /**
     * Tests findPaths when a word starting with an alien letter is passed in.  Return of an empty list is expected.
     */
    @Test
    public void findPaths_wordWithAlienFirstLetter_returnsEmptyList() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("XA");

        // verify results
        Assertions.assertEquals(0, paths.size());
    }

    /**
     * Tests findPaths when a word ending with an alien letter is passed in.  Return of an empty list is expected.
     */
    @Test
    public void findPaths_wordWithAlienTrailingLetter_returnsEmptyList() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("AX");

        // verify results
        Assertions.assertEquals(0, paths.size());
    }

    /**
     * Tests findPaths when a word which is matched once (horizontally to the right) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchHorizontalRight_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("AB");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("0:0 0:1", path.toString());
    }

    /**
     * Tests findPaths when a word which is matched once (horizontally to the left) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchHorizontalLeft_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("BA");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("0:1 0:0", path.toString());
    }

    /**
     * Tests findPaths when a word which is matched once (vertically down) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchVerticalDown_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("AC");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("0:0 1:0", path.toString());
    }

    /**
     * Tests findPaths when a word which is matched once (vertically up) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchVerticalUp_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("CA");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("1:0 0:0", path.toString());
    }
    
    /**
     * Tests findPaths when a word which is matched once (diagonally down) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchDiagonallyDown_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("AD");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("0:0 1:1", path.toString());
    }
    
    /**
     * Tests findPaths when a word which is matched once (diagonally up) is passed in.  Return of a list 
     * with a single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchDiagonallyUp_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("DA");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("1:1 0:0", path.toString());
    }
    
    /**
     * Tests findPaths when a matching word with an surrounding/embedded space is passed in.  Return of a list with a 
     * single path is expected.
     */
    @Test
    public void findPaths_wordWithSingleMatchWhitespace_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(TWO_BY_TWO_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths(" A B ");

        // verify results
        Assertions.assertEquals(1, paths.size());
        Path path = paths.get(0);
        Assertions.assertEquals("0:0 0:1", path.toString());
    }
    
    /**
     * Tests findPaths when a word is passed in but the matrix is empty.  Return of an empty list is expected.
     */
    @Test
    public void findPaths_matrixEmpty_returnsListWithOnePath() {
        // setup for test
        LetterMatrix matrix = new LetterMatrix(EMPTY_LETTERS);

        // execute test method
        List<Path> paths = matrix.findPaths("X");

        // verify results
        Assertions.assertEquals(0, paths.size());
    }
}
